<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_outlet');
            $table->bigInteger('id_member');
            $table->bigInteger('id_user');
            $table->string('invoice');
            $table->dateTime('deadline');
            $table->integer('progress')->default(0);
            $table->integer('cost');
            $table->integer('extra_cost');
            $table->integer('tax');
            $table->double('discount');
            $table->integer('payment_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
