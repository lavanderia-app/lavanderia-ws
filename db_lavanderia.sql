/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.35-MariaDB : Database - lavanderia
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`lavanderia` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `lavanderia`;

/*Table structure for table `det_transaksi` */

DROP TABLE IF EXISTS `det_transaksi`;

CREATE TABLE `det_transaksi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_transaksi` bigint(20) unsigned NOT NULL,
  `id_paket` bigint(20) unsigned NOT NULL,
  `qty` double NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_det_id_paket` (`id_paket`),
  KEY `fk_det_id_transaksi` (`id_transaksi`),
  CONSTRAINT `fk_det_id_paket` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id`),
  CONSTRAINT `fk_det_id_transaksi` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `det_transaksi` */

insert  into `det_transaksi`(`id`,`id_transaksi`,`id_paket`,`qty`,`description`,`created_at`,`updated_at`) values 
(1,4,1,1,'ini test pertama\r\n','2020-04-14 17:32:25',NULL),
(2,4,2,1,'ini test 2','2020-04-14 17:32:44',NULL),
(3,12,1,1,'1','2020-04-14 19:59:18','2020-04-14 19:59:18'),
(4,12,2,2,'2','2020-04-14 19:59:18','2020-04-14 19:59:18'),
(5,13,1,1,'Test1','2020-04-14 20:00:17','2020-04-14 20:00:17'),
(6,13,2,2,'Test2','2020-04-14 20:00:17','2020-04-14 20:00:17'),
(7,14,1,1,'Test4','2020-04-15 17:13:56','2020-04-15 17:13:56'),
(8,14,2,2,'Test5','2020-04-15 17:13:56','2020-04-15 17:13:56');

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `init_jenis_paket` */

DROP TABLE IF EXISTS `init_jenis_paket`;

CREATE TABLE `init_jenis_paket` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `init_jenis_paket` */

insert  into `init_jenis_paket`(`id`,`name`,`created_at`,`updated_at`) values 
(1,'Kiloan',NULL,NULL),
(2,'Selimut',NULL,NULL),
(3,'Bed Cover',NULL,NULL),
(4,'Kaos',NULL,NULL),
(5,'Lain-lain',NULL,NULL);

/*Table structure for table `init_progress` */

DROP TABLE IF EXISTS `init_progress`;

CREATE TABLE `init_progress` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `init_progress` */

insert  into `init_progress`(`id`,`name`,`created_at`,`updated_at`) values 
(0,'Baru',NULL,NULL),
(1,'Proses',NULL,NULL),
(2,'Selesai',NULL,NULL),
(3,'Diambil',NULL,NULL);

/*Table structure for table `init_user_role` */

DROP TABLE IF EXISTS `init_user_role`;

CREATE TABLE `init_user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `init_user_role` */

insert  into `init_user_role`(`id`,`name`,`created_at`,`updated_at`) values 
(1,'Admin',NULL,NULL),
(2,'Owner',NULL,NULL),
(3,'Kasir',NULL,NULL);

/*Table structure for table `map_paket` */

DROP TABLE IF EXISTS `map_paket`;

CREATE TABLE `map_paket` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_outlet` bigint(20) unsigned NOT NULL,
  `id_paket` bigint(20) unsigned NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_map_id_outlet` (`id_outlet`),
  KEY `fk_map_id_paket` (`id_paket`),
  CONSTRAINT `fk_map_id_outlet` FOREIGN KEY (`id_outlet`) REFERENCES `outlet` (`id`),
  CONSTRAINT `fk_map_id_paket` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `map_paket` */

insert  into `map_paket`(`id`,`id_outlet`,`id_paket`,`status`,`created_at`,`updated_at`) values 
(1,1,1,1,'2020-04-09 08:48:03','2020-04-15 17:07:01'),
(2,2,1,1,'2020-04-09 08:48:03','2020-04-15 17:02:10'),
(3,1,2,1,'2020-04-09 10:36:36','2020-04-09 10:36:36'),
(4,2,2,1,'2020-04-09 10:36:36','2020-04-09 10:36:36'),
(5,1,3,1,'2020-04-09 14:51:17','2020-04-09 14:51:17'),
(6,2,4,1,'2020-04-15 17:00:48','2020-04-15 17:00:48'),
(7,1,4,1,'2020-04-15 17:00:48','2020-04-15 17:00:48'),
(8,3,5,1,'2020-04-15 17:00:56','2020-04-15 17:00:56'),
(9,1,1,1,'2020-04-15 17:04:15','2020-04-15 17:04:15'),
(10,3,1,1,'2020-04-15 17:04:15','2020-04-15 17:04:15'),
(11,3,6,1,'2020-04-15 17:08:00','2020-04-15 17:08:00'),
(12,1,6,1,'2020-04-15 17:08:00','2020-04-15 17:08:00');

/*Table structure for table `member` */

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `member` */

insert  into `member`(`id`,`name`,`address`,`phone`,`gender`,`status`,`created_at`,`updated_at`) values 
(1,'Member 1','jalan teros','083821842596',1,1,'2020-04-14 05:38:11','2020-04-14 05:39:35'),
(2,'Member 2','jalan teros terosan','083821842596',1,1,'2020-04-15 17:08:21','2020-04-15 17:08:49');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2019_08_19_000000_create_failed_jobs_table',1),
(3,'2020_04_07_045823_create_outlet_table',1),
(4,'2020_04_07_045838_create_paket_table',1),
(5,'2020_04_07_045920_create_member_table',1),
(6,'2020_04_07_054048_create_transaksi_table',1),
(7,'2020_04_07_081828_create_det_transaksi_table',1),
(8,'2020_04_07_082108_create_map_paket_table',1),
(9,'2020_04_08_100759_create_init_user_role_table',1),
(10,'2020_04_08_100841_create_init_jenis_paket_table',1),
(11,'2020_04_08_101244_create_init_progress_table',1);

/*Table structure for table `outlet` */

DROP TABLE IF EXISTS `outlet`;

CREATE TABLE `outlet` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `outlet` */

insert  into `outlet`(`id`,`name`,`address`,`phone`,`status`,`created_at`,`updated_at`) values 
(1,'Outlet Testing','Jalan Jalan Terus','083820108171',1,'2020-04-09 07:37:17','2020-04-15 16:59:09'),
(2,'Outlet Bandung','awaokwoawokawo','+6283820108171',1,'2020-04-09 07:37:27','2020-04-15 16:58:52'),
(3,'Outlet Makassar','awaokwoawokawo','+6283820108171',1,'2020-04-15 16:58:31','2020-04-15 16:58:31');

/*Table structure for table `paket` */

DROP TABLE IF EXISTS `paket`;

CREATE TABLE `paket` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_jenis` bigint(20) unsigned NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_id_jenis` (`id_jenis`),
  CONSTRAINT `fk_id_jenis` FOREIGN KEY (`id_jenis`) REFERENCES `init_jenis_paket` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `paket` */

insert  into `paket`(`id`,`name`,`id_jenis`,`price`,`created_at`,`updated_at`,`status`) values 
(1,'Paket Cuucian',1,100000,'2020-04-09 08:48:03','2020-04-15 17:03:25',1),
(2,'Paket Hematt',1,10000,'2020-04-09 10:36:36','2020-04-09 10:36:36',1),
(3,'Paket Hemattt',1,10000,'2020-04-09 14:51:17','2020-04-09 14:51:17',1),
(4,'Paket Kiloan',1,100000,'2020-04-15 17:00:48','2020-04-15 17:00:48',1),
(5,'Paket Kiloan',1,100000,'2020-04-15 17:00:56','2020-04-15 17:00:56',1),
(6,'Paket kwintalan',1,100000,'2020-04-15 17:08:00','2020-04-15 17:08:00',1);

/*Table structure for table `transaksi` */

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_outlet` bigint(20) unsigned NOT NULL,
  `id_member` bigint(20) unsigned NOT NULL,
  `id_user` bigint(20) unsigned NOT NULL,
  `invoice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deadline` datetime NOT NULL,
  `progress` bigint(20) unsigned NOT NULL DEFAULT '0',
  `cost` int(11) NOT NULL,
  `extra_cost` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `discount` double NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_trans_id_outlet` (`id_outlet`),
  KEY `fk_trans_id_member` (`id_member`),
  KEY `fk_trans_id_user` (`id_user`),
  KEY `fk_trans_progress` (`progress`),
  CONSTRAINT `fk_trans_id_member` FOREIGN KEY (`id_member`) REFERENCES `member` (`id`),
  CONSTRAINT `fk_trans_id_outlet` FOREIGN KEY (`id_outlet`) REFERENCES `outlet` (`id`),
  CONSTRAINT `fk_trans_id_progress` FOREIGN KEY (`progress`) REFERENCES `init_progress` (`id`),
  CONSTRAINT `fk_trans_id_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `transaksi` */

insert  into `transaksi`(`id`,`id_outlet`,`id_member`,`id_user`,`invoice`,`deadline`,`progress`,`cost`,`extra_cost`,`tax`,`discount`,`payment_status`,`created_at`,`updated_at`) values 
(4,1,1,2,'abcd-efgh','2020-04-16 17:31:17',3,150000,100000,10,10,0,'2020-04-14 17:31:39','2020-04-15 17:19:49'),
(12,1,1,2,'LVNDR-14042020-1276','2020-05-16 17:31:17',0,0,0,10,0,0,'2020-04-14 19:59:17','2020-04-14 19:59:18'),
(13,1,1,2,'LVNDR-14042020-7289','2020-07-16 17:31:17',0,0,0,10,0,0,'2020-04-14 20:00:17','2020-04-14 20:00:17'),
(14,1,2,2,'LVNDR-15042020-7670','2020-07-16 17:31:17',0,132000,0,10,0,0,'2020-04-15 17:13:56','2020-04-15 17:13:56');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_outlet` bigint(20) unsigned DEFAULT NULL,
  `role` bigint(20) unsigned NOT NULL DEFAULT '3',
  `status` bigint(20) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `fk_id_role` (`role`),
  KEY `fk_id_outlet` (`id_outlet`),
  CONSTRAINT `fk_id_outlet` FOREIGN KEY (`id_outlet`) REFERENCES `outlet` (`id`),
  CONSTRAINT `fk_id_role` FOREIGN KEY (`role`) REFERENCES `init_user_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`username`,`password`,`id_outlet`,`role`,`status`,`remember_token`,`created_at`,`updated_at`) values 
(2,'Dzikri Azzakah','dazzakah','$2y$10$gwrLcMsdw9WkgLJuUcWrHuZn.rtLLO9kTtLpN7l/m2VlY.DhlLDiC',2,1,1,NULL,'2020-04-09 08:42:56','2020-04-15 18:52:41'),
(3,'Testingedit','testingedittt','$2y$10$iitVabXP0BLkS8PtnE9W5uwqnTOScaFnY9eIcYOT.IreC0tBnxDMS',2,3,1,NULL,'2020-04-15 18:25:08','2020-04-15 18:42:06');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
