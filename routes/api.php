<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register', 'Auth\Authentication@register');
Route::post('login', 'Auth\Authentication@login');
Route::post('details', 'Auth\Authentication@getAuthenticatedUser');
Route::post('logout', 'Auth\Authentication@logout');
Route::group(['middleware' => 'App\Http\Middleware\JwtMiddleware'], function ($request) {

        // Start Route For Outlet

        $request->group(array('prefix' => '/outlet'), function ($request) {
            $request->group(['middleware' => 'App\Http\Middleware\NonCashierMiddleware'], function ($request) {
                $request->post('/create', 'Api\Admin\OutletController@createOutlet')->name('createOutlet');
                $request->post('/{id}/status', 'Api\Admin\OutletController@updateStatus')->name('statusOutlet');
                $request->post('/{id}/delete', 'Api\Admin\OutletController@deleteOutlet')->name('deleteOutlet');
                $request->post('/{id}/edit', 'Api\Admin\OutletController@editOutlet')->name('editOutlet');
                $request->post('/{id}/addpaket', 'Api\Admin\OutletController@addPaket')->name('addPaketOutlet');
                $request->post('/{id}/statuspaket', 'Api\Admin\OutletController@updateStatusPaket')->name('updateStatusPaket');
                $request->post('/{id}/deletepaket', 'Api\Admin\OutletController@deletePaket')->name('deletePaketOutlet');
                $request->post('/{id}/datapaket', 'Api\Admin\OutletController@viewPaket')->name('dataAddPaket');
                $request->post('/', 'Api\Admin\OutletController@getAllOutlet')->name('allOutlet');
                $request->post('/{id}', 'Api\Admin\OutletController@detailOutlet')->name('detailOutlet');
            });   
        });

        // End Route For Outlet

        // Start Route For Paket
        
        $request->group(array('prefix' => '/paket'), function ($request) {
            $request->group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function ($request) {
                $request->group(array('prefix' => '/jenis'), function ($request) {
                    $request->post('/', 'Api\Admin\PaketController@getAllJenis')->name('allJenis');
                });
                $request->post('/create', 'Api\Admin\PaketController@createPaket')->name('createPaket');
                $request->post('/', 'Api\Admin\PaketController@getAllPaket')->name('allPaket');
                $request->post('/{id}', 'Api\Admin\PaketController@detailPaket')->name('detailPaket');
                $request->post('/{id}/delete', 'Api\Admin\PaketController@deletePaket')->name('deletePaket');
                $request->post('/{id}/status', 'Api\Admin\PaketController@updateStatus')->name('statusPaket');
                $request->post('/{id}/edit', 'Api\Admin\PaketController@editPaket')->name('editPaket');
                $request->post('/{id}/addoutlet', 'Api\Admin\PaketController@addOutlet')->name('addOutletPaket');
                $request->post('/{id}/statusoutlet', 'Api\Admin\PaketController@updateStatusOutlet')->name('updateStatusOutlet');
            });
            
        });

        // End Route For Paket


        // Start Route For Member

        $request->group(array('prefix'=> '/member'), function ($request){
            $request->post('/', 'Api\Admin\MemberController@getAllMember')->name('allMember');
            $request->post('/create', 'Api\Admin\MemberController@createMember')->name('createMember');
            $request->post('/{id}/edit', 'Api\Admin\MemberController@editMember')->name('editMember');
            $request->post('/{id}', 'Api\Admin\MemberController@detailMember')->name('detailMember');
            $request->post('/{id}/delete', 'Api\Admin\MemberController@deleteMember')->name('deleteMember');

        });
        
        // End Route For Member

        // Start Route For Transaksi

        $request->group(array('prefix'=> '/transaksi'), function ($request){
            $request->post('/', 'Api\Admin\TransaksiController@getAllTransaksi')->name('allTransaksi');
            $request->post('/create', 'Api\Admin\TransaksiController@createTransaksi')->name('createTransaksi');
            $request->post('/{id}', 'Api\Admin\TransaksiController@detailTransaksi')->name('detailTransaksi');
            $request->post('/{id}/progress', 'Api\Admin\TransaksiController@updateProgress')->name('updateProgress');
            $request->post('/{id}/payment', 'Api\Admin\TransaksiController@updatePayment')->name('updatePayment');
            $request->post('/{id}/extra', 'Api\Admin\TransaksiController@extraCost')->name('extraCost');
        });

        // End Route For Transaksi

        // Start Route For User

        $request->group(array('prefix'=> '/user'), function ($request){
            $request->post('/changepassword', 'Api\Admin\UserController@changePassword')->name('changePassword');
            $request->group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function ($request) {
                $request->post('/kasir', 'Api\Admin\UserController@getAllKasir')->name('allKasir');
                $request->post('/{id}/delete', 'Api\Admin\UserController@deleteKasir')->name('deleteKasir');
                $request->post('/changeoutlet', 'Api\Admin\UserController@changeOutlet')->name('changeOutlet');
                $request->post('/edit', 'Api\Admin\UserController@editProfile')->name('editProfile');
                $request->post('/{id}', 'Api\Admin\UserController@detailKasir')->name('detailKasir');
            });
        });

        // End Route For User
});
