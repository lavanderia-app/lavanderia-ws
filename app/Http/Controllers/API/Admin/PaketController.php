<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Paket;
use App\Models\Outlet;
use App\Models\Map_Paket;
use App\Models\InitJenisPaket;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Illuminate\Support\Facades\DB;

class PaketController extends Controller
{
    public function getAllPaket(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user->role <= 2) {

            $dataSource = [];

            $paket = Paket::where('status' , '>=', 0)->get();

            foreach ($paket as $key => $value) {
                $data['id'] = $value->id;
                $data['name'] = $value->name;
                $jenis = InitJenisPaket::where('id', $value->id_jenis)->get();
                $data['jenis'] = $jenis->get('name');
                $data['price'] = $value->price;
                $outletSource = [];
                $mapPaket = Map_Paket::where('id_paket', $value->id)->get();
                foreach ($mapPaket as $key => $valueM) {
                    $outlet = Outlet::where('id', $valueM->id_outlet)->get();
                    foreach ($outlet as $key => $valueO) {
                        $dataO['id'] = $valueO->id;
                        $dataO['name'] = $valueO->name;
                        array_push($outletSource, $dataO);
                    }
                }
                $data['outlet'] = $outletSource;
                array_push($dataSource, $data);
            }
            return $dataSource;
            
        } elseif ($user->role == 3) {
            $dataSource = [];
            
            $mapPaket = Map_Paket::where('id_outlet', $user->id_outlet)->where('status', '>', 0)->get();
            foreach ($mapPaket as $key => $value) {
                $paket = Paket::where('id', $value->id_paket)->first();
                $data['id'] = $paket->id;
                $data['name'] = $paket->name;
                $data['price'] = $paket->price;
                $jenis = InitJenisPaket::where('id', $paket->id_jenis)->first();
                $data['jenis'] = $jenis->name;
            }
            array_push($dataSource, $data);
            
            return $dataSource;
        }
    }

    public function createPaket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'id_jenis' => 'required|integer',
            'price' => 'required|integer',
            'id_outlet' => 'required'
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        
        $outlet = $request->id_outlet;
        
        DB::beginTransaction();
        try{
            $paket = Paket::create([
                'name' => $request->name,
                'id_jenis' => $request->id_jenis  ,
                'price' => $request->price
            ]);
            $mapPaket = [];
            foreach ($outlet as $key => $value) {
                $mapping = Map_Paket::create([
                    'id_paket' => $paket->id,
                    'id_outlet' => $value
                ]);
                $mappingArray = $mapping->toArray();
                array_push($mapPaket, $mappingArray);
            }
            DB::commit();
            return response()->json(compact('paket', 'mapPaket'));
        } catch (\Exception $e) {
            return $e;
            DB::rollback();
        }

    }

    public function detailPaket(Request $request, $id)
    {
        
        $paket = Paket::find($id);

        if (!$paket) {
            return $this->sendError(null, 'not found', 404);
        }

        $dataSource = [];
        $data['id'] = $paket->id;
        $data['name'] = $paket->name;
        $data['price'] = $paket->price;
        $jenis = InitJenisPaket::where('id', $paket->id_jenis)->first();
        $data['jenis'] = $jenis->name;
        $outletSource = [];
        $mapPaket = Map_Paket::where('id_paket',$id)->get();
        foreach ($mapPaket as $key => $valueM) {
            $outlet = Outlet::where('id', $valueM->id_outlet)->first();
            $dataO['id'] = $valueM->id;
            $dataO['name'] = $outlet->name;
            $dataO['status'] = $valueM->status;
            array_push($outletSource, $dataO);
        }
        $data['outlet'] = $outletSource;

        array_push($dataSource, $data);

        return $dataSource;
    }

    public function editPaket(Request $request, $id)
    {
        $data = $request->all();
        $paket = Paket::find($id);

        if (!$paket) {
            return $this->sendError(null, 'Paket Not Found', 404);
        }

        if ($request->has('name')) {
            $paket->name = $request->name ;
        }

        if ($request->has('id_jenis')) {
            $paket->id_jenis = $request->id_jenis ;
        }

        if ($request->has('price')) {
            $paket->price = $request->price ;
        }

        $paket->save();
        return response()->json(compact('paket'));
    }

    public function addOutlet(Request $request, $id)
    {
        $paket = Paket::find($id);
        $outlet = $request->id_outlet;

        $mapPaket = [];
        foreach ($outlet as $key => $value) {
            $mapping = Map_Paket::create([
                'id_outlet' => $value,
                'id_paket' => $paket->id
            ]);
            $mappingArray = $mapping->toArray();
            array_push($mapPaket, $mappingArray);
        }

        return response()->json(compact('paket', 'mapPaket'));
    }

    public function updateStatusOutlet(Request $request,$id)
    {
        $mapPaket = Map_paket::find($id);

        if ($mapPaket->status == 1) {
            $mapPaket->status = 0;
            $mapPaket->save();
            $message = "Berhasil Update";
            return response()->json(
                [
                  'success' => true,
                  'code' => 200,
                  'data' => $mapPaket,
                  'message' => $message
                ]
              );

        } elseif ($mapPaket->status == 0) {
            $mapPaket->status = 1;
            $mapPaket->save();
            $message = "Berhasil Update";
            return response()->json(
                [
                  'success' => true,
                  'code' => 200,
                  'data' => $mapPaket,
                  'message' => $message
                ]
              );
        }
    }

    public function updateStatus(Request $request, $id)
    {
        DB::beginTransaction();
        try{
        $paket = Paket::find($id);
        if (!$paket) {
            return $this->sendError(null, 'Paket Not Found', 404);
        }
        
        $mapPaket= Map_paket::where('id_paket', $id)->get();
        if (!$mapPaket) {
            return $this->sendError(null, 'Map Paket Not Found', 404);
        }

        if ($paket->status == 1) {
            $paket->status = '0';
            $paket->save();
            foreach ($mapPaket as $key => $value) {
                $value->status = 0;
                $value->save();
            }
            DB::commit();
            $message = 'Paket Nonaktif';
        } 
        
        elseif ($paket->status == 0) {
            $paket->status = '1';
            $paket->save();
            foreach ($mapPaket as $key => $value) {
                $value->status = 1;
                $value->save();
            }
            DB::commit();
            $message = 'Paket Aktif';
        }

        return response()->json(
            [
              'success' => true,
              'code' => 200,
              'data' => $paket,
              'message' => $message
            ]
          );
        }catch(\Exception $e){
            return $e;
            DB::rollback();
        }
    }

    public function deletePaket(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $paket = Paket::find($id);
            if (!$paket) {
                return $this->sendError(null, 'paket not found', 404);
            }

            $mapPaket= Map_paket::where('id_paket', $id)->get();
            if (!$mapPaket) {
                return $this->sendError(null, 'map paket not found', 404);
            }
            
            $paket->status = -1;
            $paket->save();
            foreach ($mapPaket as $key => $value) {
                $value->status = -1;
                $value->save();
            }
            $message = "Berhasil Menghapus Paket";
            DB::commit();

            return response()->json(
                [
                  'success' => true,
                  'code' => 200,
                  'data' => $paket,
                  'message' => $message
                ]
              );

        }catch (\Exception $e) {
            return $e;
            DB::rollback();
        }
        
    }

    public function getAllJenis(Request $request)
    {
        $item = InitJenisPaket::all();
        return response()->json($item, 200);
    }

    
}
