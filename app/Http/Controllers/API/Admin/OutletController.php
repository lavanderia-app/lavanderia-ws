<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Outlet;
use App\Models\Map_Paket;
use App\Models\Paket;
use App\Models\InitJenisPaket;
use Illuminate\Support\Facades\Validator;

class OutletController extends Controller
{
    public function getAllOutlet()
    {
        $item = Outlet::where('status' ,'>=', 0)->get();
        return response()->json($item, 200);
    }

    public function editOutlet(Request $request, $id)
    {
        $outlet = Outlet::find($id);
        
        if (!$outlet) {
            return $this->sendError(null, 'not found', 404);
        }

        if ($request->has('name')) {
            $outlet->name = $request->name;
        }

        if ($request->has('address')) {
            $outlet->address = $request->address;
        }

        if ($request->has('phone')) {
            $outlet->phone = $request->phone;
        }

        $outlet->save();
        return response()->json(compact('outlet'));
        
    }

    public function createOutlet(Request $request)
    { 
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $outlet = Outlet::create([
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone
        ]);

        return response()->json(compact('outlet'));
    }

    public function updateStatusPaket(Request $request, $id)
    {
        $mapPaket = Map_paket::find($id);
        
        if ($mapPaket->status == 1) {
            $mapPaket->status = 0;
            $mapPaket->save();
            $message = "Berhasil Update";
            return response()->json(
                [
                  'success' => true,
                  'code' => 200,
                  'data' => $mapPaket,
                  'message' => $message
                ]
              );

        } elseif ($mapPaket->status = 0) {
            $mapPaket->status = 1;
            $mapPaket->save();
            $message = "Berhasil Update";
            return response()->json(
                [
                  'success' => true,
                  'code' => 200,
                  'data' => $mapPaket,
                  'message' => $message
                ]
              );
        }
    }

    public function deletePaket(Request $request, $id)
    {
        $mapPaket = Map_Paket::find($id);
        $mapPaket->status = -1;
        $mapPaket->save();
        $message = "Paket berhasil dihapus";

        return response()->json(
            [
              'success' => true,
              'code' => 200,
              'data' => $mapPaket,
              'message' => $message
            ]
          );
    }

    public function detailOutlet(Request $request, $id)
    {
        $dataSource = [];
        $item = Outlet::find($id);

        $data['id'] = $item->id;
        $data['name'] = $item->name;
        $data['address'] = $item->address;
        $data['phone'] = $item->phone;

        if (!$item) {
            return $this->sendError(null, 'Outlet not found', 404);
        }
    
        $dataPaket = [];
        $mapPaket = Map_Paket::where('id_outlet', $id )->where('status' , '>=', 0)->get();
        foreach ($mapPaket as $key => $value) {
            $paket = Paket::where('id', $value->id_paket)->first();
            $dataP['id'] = $value->id;
            $dataP['name'] = $paket->name;
            $dataP['price'] = $paket->price;
            $dataP['status'] = $value->status;
            $jenis = InitJenisPaket::where('id', $paket->id_jenis)->first();
            $dataP['jenis'] = $jenis->name;
            array_push($dataPaket, $dataP); 
        }
        $data['paket'] = $dataPaket;

        array_push($dataSource, $data);
        
        return $dataSource;
        
    }

    public function viewPaket(Request $request, $id)
    {
        $mapPaket = Map_Paket::where('id_outlet' , '!=', $id)->groupBy('id_paket')->get();
        $dataSource = [];
        foreach ($mapPaket as $key => $value) {
            $paket = Paket::where('id', $value->id_paket)->first();
            $data['id'] = $paket->id;
            $data['name'] = $paket->name;
            $data['price'] = $paket->price;

            array_push($dataSource, $data);
        }

        return $dataSource;
    }

    public function addPaket(Request $request, $id)
    {
        $outlet = Outlet::find($id)->first();
        $paket = $request->id_paket;
        $mapPaket = [];
            foreach ($paket as $key => $value) {
                $mapping = Map_Paket::create([
                    'id_paket' => $value,
                    'id_outlet' => $outlet->id
                ]);
                $mappingArray = $mapping->toArray();
                array_push($mapPaket, $mappingArray);
            }
        return response()->json(compact('paket', 'mapPaket'));
    }
    
    public function updateStatus(Request $request, $id)
    {
        $outlet = Outlet::find($id);

        if (!$outlet) {
            return $this->sendError(null, 'not found', 404);
        }

        if ($outlet->status == '1') {
            $outlet->status = '0';
            $outlet->save();
            $message = 'Outlet Nonaktif';
        }

        else if ($outlet->status == '0') {
            $outlet->status = '1';
            $outlet->save();
            $message = 'Outlet Aktif';
        }

        return response()->json(
            [
              'success' => true,
              'code' => 200,
              'data' => $outlet,
              'message' => $message
            ]
          );
    }

    public function deleteOutlet(Request $request, $id)
    {
        $outlet = Outlet::find($id);

        if (!$outlet) {
            return $this->sendError(null, 'not found', 404);
        }
     
            $outlet->status = '-1';
            $outlet->save();
        
        return response()->json(
            [
              'success' => true,
              'code' => 200,
              'data' => $outlet,
              'message' => 'Berhasil Menghapus'
            ]
          );
          
    }


}
