<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Member;
use Illuminate\Support\Facades\Validator;   


class MemberController extends Controller
{
    public function getAllMember(Request $request)
    {
        $member = Member::where('status', '>=', '1')->get();
        $dataSource = [];
        foreach ($member as $key => $value) {
            $data['id'] = $value->id;
            $data['name'] = $value->name;
            $data['address'] = $value->address;
            $data['phone'] = $value->phone;
            $data['gender'] = $value->gender;

            array_push($dataSource, $data);
        }

        return $dataSource;
    }

    public function createMember(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|string',
            'gender' => 'required|integer'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $member = Member::create([
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'gender' => $request->gender
        ]);

        return response()->json($member, 200);

    }

    public function editMember(Request $request, $id)
    {
        $member = Member::find($id);

        if (!$member) {
            return $this->sendError(null, 'not found', 404);
        }

        if ($request->has('address')) {
            $member->address = $request->address;
        }

        if ($request->has('phone')) {
            $member->phone = $request->phone;
        }

        $member->save();
        return response()->json($member, 200);
    }

    public function detailMember(Request $request, $id)
    {
        $member = Member::find($id);
        if (!$member) {
            return $this->sendError(null, 'not found', 404);
        }

        return response()->json($member, 200);
    }

    public function deleteMember(Request $request, $id)
    {
        $member = Member::find($id);
        
        if (!$member) {
            return $this->sendError(null, 'not found', 404);
        }

        $member->status = -1;

        $member->save();

        return response()->json($member, 200);
    }

}
