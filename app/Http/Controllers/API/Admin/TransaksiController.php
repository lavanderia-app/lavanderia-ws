<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Models\Transaksi;
use App\Models\Det_Transaksi;
use App\Models\Member;
use App\Models\Paket;
use App\Models\Outlet;
use App\Models\InitProgress;
use App\Models\InitJenisPaket;
use App\User;



class TransaksiController extends Controller
{
    public function getAllTransaksi(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user->role == 3) {
            $dataSource = [];
            $transaksi = Transaksi::where('id_outlet', $user->id_outlet)->orderBy('progress', 'asc')->get();
            foreach ($transaksi as $key => $value) {
                $data['id'] = $value->id;
                $data['invoice'] = $value->invoice;
                $user = User::where('id', $value->id_user)->first();
                $data['user'] = $user->name;
                $member = Member::where('id', $value->id_member)->first();
                $data['member'] = $member->name;
                $progress = InitProgress::where('id', $value->progress)->first();
                $data['progress'] = $progress->name;
                $data['deadline'] = $value->deadline;
                $data['cost'] = $value->cost;
                $data['extra_cost'] = $value->extra_cost;
                $data['tax'] = $value->tax;
                $data['discount'] = $value->discount;
                $data['payment_status'] = $value->payment_status;
                $det_transaksi = Det_Transaksi::where('id_transaksi', $value->id)->get();
                $dataTransaksi = [];
                foreach ($det_transaksi as $key => $valueT) {
                    $paket = Paket::where('id', $valueT->id_paket)->first();
                    $dataT['paket'] = $paket->name;
                    $dataT['qty'] = $valueT->qty;
                    $dataT['description'] = $valueT->description;

                    array_push($dataTransaksi, $dataT);
                }
                $data['det_transaksi'] = $dataTransaksi;

                array_push($dataSource, $data);
                
            }

        }elseif ($user->role <= 2) {
            
            $dataSource = [];
            $transaksi = Transaksi::orderBy('progress', 'asc')->get();
            foreach ($transaksi as $key => $value) {
                $data['id'] = $value->id;
                $data['invoice'] = $value->invoice;
                $outlet = Outlet::where('id', $value->id_outlet)->first();
                $data['outlet'] = $outlet->name;
                $user = User::where('id', $value->id_user)->first();
                $data['user'] = $user->name;
                $member = Member::where('id', $value->id_member)->first();
                $data['member'] = $member->name;
                $progress = InitProgress::where('id', $value->progress)->first();
                $data['progress'] = $progress->name;
                $data['deadline'] = $value->deadline;
                $data['cost'] = $value->cost;
                $data['extra_cost'] = $value->extra_cost;
                $data['tax'] = $value->tax;
                $data['discount'] = $value->discount;
                $data['payment_status'] = $value->payment_status;
                $det_transaksi = Det_Transaksi::where('id_transaksi', $value->id)->get();
                $dataTransaksi = [];
                foreach ($det_transaksi as $key => $valueT) {
                    $paket = Paket::where('id', $valueT->id_paket)->first();
                    $dataT['paket'] = $paket->name;
                    $dataT['qty'] = $valueT->qty;
                    $dataT['description'] = $valueT->description;

                    array_push($dataTransaksi, $dataT);
                }
                $data['det_transaksi'] = $dataTransaksi;

                array_push($dataSource, $data);
                
            }

        }

        return $dataSource;
    }

    public function detailTransaksi(Request $request, $id)
    {
        $transaksi = Transaksi::find($id);
        if (!$transaksi) {
            return $this->sendError(null, 'Not Found', 404);
        }
        $dataSource = [];
        $data['id'] = $transaksi->id;
        $data['invoice'] = $transaksi->invoice;
        $user = User::where('id', $transaksi->id_user)->first();
        $data['user'] = $user->name;
        $member = Member::where('id', $transaksi->id_member)->first();
        $data['member'] = $member->name;
        $progress = InitProgress::where('id', $transaksi->progress)->first();
        $data['progress'] = $progress->name;
        $data['deadline'] = $transaksi->deadline;
        $data['cost'] = $transaksi->cost;
        $data['extra_cost'] = $transaksi->extra_cost;
        $data['tax'] = $transaksi->tax;
        $data['discount'] = $transaksi->discount;
        $data['payment_status'] = $transaksi->payment_status;
        $det_transaksi = Det_Transaksi::where('id_transaksi', $id)->get();
        $dataTransaksi = [];
        foreach ($det_transaksi as $key => $valueT) {
            $paket = Paket::where('id', $valueT->id_paket)->first();
            $dataT['paket'] = $paket->name;
            $dataT['qty'] = $valueT->qty;
            $dataT['description'] = $valueT->description;

            array_push($dataTransaksi, $dataT);
        }
        $data['det_transaksi'] = $dataTransaksi;

        array_push($dataSource, $data);
        return $dataSource;

    }

    public function updateProgress(Request $request, $id)
    {
        $transaksi = Transaksi::find($id);

        $latprogress = $transaksi->progress;

        if ($transaksi->progress !=3) {
            $transaksi->progress = $latprogress+1;
            $transaksi->save();
        }        
        

        return $transaksi;
    
    }

    public function updatePayment(Request $request, $id)
    {
        $transaksi = Transaksi::find($id);
        $latpembayaran = $transaksi->payment_status;
        if ($transaksi->progress !=1) {
        $transaksi->progress = $latprogress+1;
        $transaksi->save();
        }
        return $transaksi;
    }



    public function createTransaksi(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user->role == 3) {
            $validator = Validator::make($request->all(), [
                // 'id_outlet' => 'required|integer' ,
                'id_member' => 'required|integer' ,
                'deadline' => 'required|date',
            ]);
            
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }

            DB::beginTransaction();
            
            try{

                $invoice = "LVNDR-".date("dmY")."-".mt_rand(1000,9999);
                $transaksi = Transaksi::create([
                    'id_outlet' => $user->id_outlet,
                    'id_member' => $request->id_member,
                    'id_user' => $user->id,
                    'invoice' => $invoice,
                    'deadline' => $request->deadline,
                ]);

                $detTransaksi = $request->detTransaksi;
                
                $dataDetTransaksi = [];

                foreach ($detTransaksi as $key => $value) {
                    $createDetTransaksi = Det_Transaksi::create([
                        'id_transaksi' => $transaksi->id,
                        'id_paket' => $value['id_paket'],
                        'qty' => $value['qty'],
                        'description' => $value['description']
                    ]);
                    
                    array_push($dataDetTransaksi, $createDetTransaksi);
                }

                $cost = 0;
                $totalDiscount = 0;
                $totalTax = 0;
                $tax = 0.1;
                $discount = $request->discount;
                $totalCost = 0;

                if ($discount == 0) {
                    foreach ($dataDetTransaksi as $key => $dataDetTran) {
                        $paket = Paket::where('id', $dataDetTran->id_paket)->first();
                        $cost += $paket->price*$dataDetTran->qty;
                    }
                    
                    $totalTax = $cost * $tax;
                    $totalCost = $cost + $totalTax;
                } elseif ($discount != 0) {
                    foreach ($dataDetTransaksi as $key => $dataDetTran) {
                        $paket = Paket::where('id', $dataDetTran->id_paket)->first();
                        $cost += $paket->price*$paket->qty;
                    }
                    $totalDiscount = $cost * $discount / 100;
                    $totalTax = $cost * $tax;
                    $totalCost = $cost + $totalTax - $totalDiscount;
                }
                $transaksi->cost = $totalCost;
                $transaksi->tax = $tax*100;
                $transaksi->discount = $request->discount;
                $transaksi->save();
                
                DB::commit();
            
            }catch(Exception $e){
                
                return $e;
                DB::rollback();

            }

        }elseif ($user->role ==1 ) {
            
            $validator = Validator::make($request->all(), [
                'id_outlet' => 'required|integer' ,
                'id_member' => 'required|integer' ,
                'deadline' => 'required|date',
            ]);
            
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }

            DB::beginTransaction();
            
            try{

                $invoice = "LVNDR-".date("dmY")."-".mt_rand(1000,9999);
                $transaksi = Transaksi::create([
                    'id_outlet' => $request->id_outlet,
                    'id_member' => $request->id_member,
                    'id_user' => $user->id,
                    'invoice' => $invoice,
                    'deadline' => $request->deadline,
                ]);

                $detTransaksi = $request->detTransaksi;
                
                $dataDetTransaksi = [];

                foreach ($detTransaksi as $key => $value) {
                    $createDetTransaksi = Det_Transaksi::create([
                        'id_transaksi' => $transaksi->id,
                        'id_paket' => $value['id_paket'],
                        'qty' => $value['qty'],
                        'description' => $value['description']
                    ]);
                    
                    array_push($dataDetTransaksi, $createDetTransaksi);
                }

                $cost = 0;
                $totalDiscount = 0;
                $totalTax = 0;
                $tax = 0.1;
                $discount = $request->discount;
                $totalCost = 0;

                if ($discount == 0) {
                    foreach ($dataDetTransaksi as $key => $dataDetTran) {
                        $paket = Paket::where('id', $dataDetTran->id_paket)->first();
                        $cost += $paket->price*$dataDetTran->qty;
                    }
                    
                    $totalTax = $cost * $tax;
                    $totalCost = $cost + $totalTax;
                } elseif ($discount != 0) {
                    foreach ($dataDetTransaksi as $key => $dataDetTran) {
                        $paket = Paket::where('id', $dataDetTran->id_paket)->first();
                        $cost += $paket->price*$paket->qty;
                    }
                    $totalDiscount = $cost * $discount / 100;
                    $totalTax = $cost * $tax;
                    $totalCost = $cost + $totalTax - $totalDiscount;
                }
                $transaksi->cost = $totalCost;
                $transaksi->tax = $tax*100;
                $transaksi->discount = $request->discount;
                $transaksi->save();
                
                DB::commit();
            
            }catch(Exception $e){
                
                return $e;
                DB::rollback();

            }
        }

    }

    public function extraCost(Request $request, $id)
    {
        $transaksi = Transaksi::find($id);

        if (!$transaksi) {
            return $this->sendError(null, 'Not Found', 404);
        }

        $latCost = $transaksi->cost;
        $extraCost = $request->extraCost;

        $totCost = $latCost + $extraCost;

        $transaksi->cost = $totCost;
        $transaksi->extra_cost = $extraCost;

        $transaksi->save();

        return $transaksi;
    }
}
