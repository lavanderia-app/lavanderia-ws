<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Outlet;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Transaksi;
use JWTAuth;

class UserController extends Controller
{
    
    public function getAllKasir(Request $request)
    {
        $kasir = User::where('role', 3)->get();
        $dataSource = [];

        foreach ($kasir as $key => $value) {
            $data['name'] = $value->name;
            $data['username'] = $value->username;
            $outlet = Outlet::where('id', $value->id_outlet)->first();
            $data['outlet'] = $outlet->name;
            $data['status'] = $value->status;

            array_push($dataSource, $data);
        }

        return $dataSource;
    }

    public function detailKasir(Request $request, $id)
    {
        $kasir = User::find($id);
        if (!$kasir) {
            return $this->sendError(null, 'not found', 404);
        }

        return response()->json(compact('kasir'));
    }
    

    public function changePassword(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $recent = Hash::check($request->rec_password, $user->password);

        if (!$recent) {
            return $this->sendError(null, 'Password not match', 400);
        }

        $password = Hash::make($request->get('password'));

        $user->password = $password;
        $user->save();

        return $this->sendSuccess($user, 'Success', 200);
    }

    public function editProfile(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($request->has('id_user')) {
            $user = User::where('id',$request->id_user)->first();
        }
        
        if ($request->has('name')) {
            $user->name = $request->name;
        }

        if ($request->has('username')) {
            $user->username = $request->username;
        }
        
        $user->save();
        
        return $this->sendSuccess($user, 'Success', 200);
    }

    public function changeOutlet(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($request->has('id_user')) {
            $user = User::where('id',$request->id_user)->first();
        }
        
        $user->id_outlet = $request->id_outlet;
        $user->save();

        return $this->sendSuccess($user, 'Success', 200);
    }

    public function deleteKasir(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            return $this->sendError(null, 'User Not Found', 404);
        }

        $user->status = -1;
        $user->save();

        return $this->sendSuccess($user, 'Success', 200);
    }

}
