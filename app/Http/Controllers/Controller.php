<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendSuccess($data, $message = '')
    {
      return response()->json([
        'success' => true,
        'data' => $data,
        'errors' => null,
        'message' => $message
      ]);
    }

    public function sendError($data, $message = '', $code = 400)
    {
      return response([
        'success' => false,
        'data' => null,
        'errors' => $data,
        'message' => $message
      ], $code);
    }
}
