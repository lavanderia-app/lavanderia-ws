<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::parseToken()->authenticate();
         if ($user->role != 1) {
            return $this->sendError([
                'token' => 'Unauthorized'
              ], 401);
         }
         return $next($request);
    }

    public function sendError($data, $message = '', $code = 400)
    {
      return response()->json([
        'success' => false,
        'data' => null,
        'errors' => $data,
        'message' => $message
      ], 401);
    }
}
