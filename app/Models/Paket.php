<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    protected $table = "paket";
    
    protected $fillable = ["name", "id_jenis", "price", "created_at"];

    public function jenis()
    {
        return $this->belongsTo('App\Models\InitJenisPaket','id_jenis','id');
    }

}
