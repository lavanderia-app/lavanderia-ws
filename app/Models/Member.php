<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = "member";
    
    protected $fillable = ["name", "address", "phone", "gender", "status", "created_at"];
}
