<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InitJenisPaket extends Model
{
    protected $table = "init_jenis_paket";
    
    protected $fillable = ['name'];
}
