<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InitProgress extends Model
{
    protected $table = "init_progress";
    
    protected $fillable = ['name'];
}
