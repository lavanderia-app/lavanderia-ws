<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Map_Paket extends Model
{
    protected $table = "map_paket";

    protected $fillable = ["id_outlet", "id_paket", "status",'created_at'];

    public function outlet() {
        return $this->belongsTo(\App\Models\Outlet::class, 'id_outlet', 'id');
    }

    public function paket() {
        return $this->belongsTo(\App\Models\Paket::class, 'id_paket', 'id');
    }
}
