<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Det_transaksi extends Model
{
    protected $table = "det_transaksi";
    
    protected $fillable = ["id_transaksi", "id_paket", "qty", "description", 'created_at'];

    public function paket()
    {
        return $this->belongsTo('App\Models\Paket', 'id_paket', 'id');
    }

    public function transaksi()
    {
        return $this->belongsTo('App\Models\Transaksi','id_transaksi','id');
    }
}
