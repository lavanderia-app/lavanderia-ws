<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = "transaksi";
    
    protected $fillable = ["id_outlet", "id_member", "id_user", "invoice", "deadline", "progress", "cost", "extra_cost", "tax", "discount", "payment_status", "created_at"];

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'id');
    }

    public function outlet()
    {
        return $this->belongsTo('App\Models\Outlet', 'id_outlet', 'id');
    }

    public function member()
    {
        return $this->belongsTo('App\Models\Member', 'id_member', 'id');
    }

}
